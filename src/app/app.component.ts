import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(["login"])
  }

  public static fade(acao: string, tempo: number, elemento: string): void {
    if (acao == "in") {
      $(elemento).css("opacity", "0")
      $(elemento).show();
      setTimeout(() => {
        $(elemento).css("opacity", "100%")
      }, tempo)
    }
    else if (acao == "out") {
      $(elemento).css("opacity", "0")
      setTimeout(() => {
        $(elemento).hide();
      }, tempo)
    }
  }

  public static abreLoading() {
    $("#modalLoading").modal("show");
  }

  public static fechaLoading() {
    setTimeout(() => {
      $("#modalLoading").modal("hide");
    }, 500)
  }

}
